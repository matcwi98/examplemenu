import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { Menu } from "./pages/Menu";
import { Home } from "./pages/Home";
import { ChooseLanguage } from "./pages/ChooseLanguage";

const Routes = () => {
  return (
    <Router>
      <Switch>
        <Route exact path="/">
          <Home />
        </Route>
        <Route path="/wybierz-jezyk">
          <ChooseLanguage />
        </Route>
        <Route path="/przykladowe-menu">
          <Menu />
        </Route>
      </Switch>
    </Router>
  );
};

export default Routes;
