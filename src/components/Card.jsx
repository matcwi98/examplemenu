import React from "react";
import styled from "styled-components";
import "react-toggle/style.css";
import { ReactComponent as PlusIcon } from "../assets/icons/plus.svg";
import { ReactComponent as MinusIcon } from "../assets/icons/minus.svg";

const CardContainer = styled.div`
  max-width: 100%;
  box-shadow: 0 7px 30px -10px rgba(150, 170, 180, 0.5);
  background-color: ${(props) => (props.isSelected ? "#fee715ff" : "#fff")};
  margin-top: 20px;
  padding: 20px;
  border-radius: 10px;
`;

const DetailsSection = styled.div`
  display: flex;
  flex-flow: column wrap;
  justify-content: flex-start;
  align-items: flex-start;
  margin-right: 10px;
`;

const PriceSection = styled.div`
  display: flex;
  flex-flow: column wrap;
  justify-content: flex-start;
  align-items: center;
  font-weight: 800;
  margin-left: 10px;
  font-size: 20px;
`;

const CardSectionsWrapper = styled.div`
  display: flex;
  flex-flow: row nowrap;
  justify-content: space-between;
`;

const Title = styled.div`
  font-weight: 600;
  font-size: 20px;
  letter-spacing: 1px;
  text-align: initial;
  margin-bottom: 10px;
`;

const Description = styled.div`
  font-weight: 300;
  font-size: 14px;
  text-align: initial;
`;

const Card = ({
  title,
  description,
  price,
  addToCart,
  removeFromCart,
  id,
  onCart,
}) => {
  const [isSelected, setIsSelected] = React.useState(onCart);

  const handleChange = () => {
    if (!isSelected) {
      addToCart(id);
    } else {
      removeFromCart(id);
    }
    setIsSelected((isSelected) => !isSelected);
  };

  return (
    <CardContainer isSelected={isSelected}>
      <CardSectionsWrapper>
        <DetailsSection>
          <Title>
            {id}. {title}
          </Title>
          <Description>{description}</Description>
        </DetailsSection>
        <PriceSection>
          <span style={{ marginBottom: "10px" }}>{price}</span>
          {!onCart && (
            <span style={{ marginTop: "auto" }}>
              {!isSelected ? (
                <PlusIcon width="30px" height="30px" onClick={handleChange} />
              ) : (
                <MinusIcon width="30px" height="30px" onClick={handleChange} />
              )}
            </span>
          )}
        </PriceSection>
      </CardSectionsWrapper>
    </CardContainer>
  );
};

export default Card;
