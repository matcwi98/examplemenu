import React from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";
import { ReactComponent as PolandFlagIcon } from "../assets/icons/flagPoland.svg";
import { ReactComponent as EngFlagIcon } from "../assets/icons/uk.svg";
import { ReactComponent as ChinaFlagIcon } from "../assets/icons/china.svg";
import { useTranslation } from "react-i18next";

const Layout = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  min-height: 100vh;
  flex-flow: row wrap;
  background: #101820ff;

  @media (max-width: 600px) {
    flex-flow: column wrap;
  }
`;

const Item = styled.div`
  display: flex;
  flex-flow: column nowrap;
  width: 100px;
  justify-content: center;
  align-items: center;
  margin: 20px;
  &:hover {
    border-bottom: 3px solid #fee715ff;
  }
  @media (max-width: 600px) {
    padding: 0;
  }
`;

export const ChooseLanguage = () => {
  const [t, i18n] = useTranslation("common");

  return (
    <Layout>
      <Item onClick={() => i18n.changeLanguage("pol")}>
        <Link
          style={{ textDecoration: "none", lineHeight: "40px" }}
          to="/przykladowe-menu?lng=pol"
        >
          <PolandFlagIcon width="100px" height="100px" />
        </Link>
      </Item>
      <Item onClick={() => i18n.changeLanguage("en")}>
        <Link
          style={{ textDecoration: "none", lineHeight: "40px" }}
          to="/przykladowe-menu?lng=en"
        >
          <EngFlagIcon width="100px" height="100px" />
        </Link>
      </Item>
      <Item onClick={() => i18n.changeLanguage("zh")}>
        <Link
          style={{ textDecoration: "none", lineHeight: "40px" }}
          to="/przykladowe-menu"
        >
          <ChinaFlagIcon width="100px" height="100px" />
        </Link>
      </Item>
    </Layout>
  );
};
