import React from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";

const Container = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 100vh;
  background: #101820ff;
`;

const Title = styled.a`
  text-decoration: none;
  border: 1px solid #fee715ff;
  padding: 30px;
  font-size: 20px;

  &:hover {
    color: #101820ff;
    background: #fee715ff;
    transform: scale(1.1);
  }

  transition: 0.5s ease-in-out;
`;

export const Home = () => (
  <Container>
    <Link to="/wybierz-jezyk" style={{ color: "black" }}>
      <Title>Menu</Title>
    </Link>
  </Container>
);
