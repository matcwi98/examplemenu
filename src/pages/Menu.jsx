import React from "react";
import styled from "styled-components";
import Card from "../components/Card";
import "./Menu.css";
import { Element, scroller, animateScroll as scroll } from "react-scroll";
import up from "../assets/icons/up.svg";
import AOS from "aos";
import "aos/dist/aos.css";
import { ReactComponent as MeatIcon } from "../assets/icons/burger.svg";
import { ReactComponent as DrinkIcon } from "../assets/icons/drink.svg";
import { ReactComponent as FishIcon } from "../assets/icons/fish.svg";
import { ReactComponent as SoupIcon } from "../assets/icons/soup.svg";
import { ReactComponent as StarterIcon } from "../assets/icons/starter.svg";
import { Link } from "react-router-dom";
import { useTranslation } from "react-i18next";
import { ReactComponent as CartIcon } from "../assets/icons/buy.svg";
import { meals } from "../data/Meals";

const ScrollToTopIcon = styled.span`
  position: fixed;
  bottom: 30px;
  right: 20px;
  width: 70px;
  height: 70px;
  cursor: pointer;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const Layout = styled.div`
  width: 100%;
  min-height: 100vh;
  background-color: #101820ff;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-flow: column nowrap;
`;

const CategoriesContainer = styled.div`
  display: flex;
  flex-flow: row wrap;
  justify-content: center;
  padding: 60px;
  align-items: center;
  margin-top: 50px;
`;

const MealTypeContainer = styled.div`
  width: 100%;
`;

const Header = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  background: #fee715ff;
  width: 100%;
  height: 50px;
  position: fixed;
  top: 0;
  z-index: 10;
`;

const MenuContentContainer = styled.div`
  padding: 0 150px 40px 150px;
  @media (max-width: 1150px) {
    padding: 0 100px 40px 100px;
  }
  @media (max-width: 600px) {
    padding: 0 40px 40px 40px;
  }
`;

const DishesTypeTitle = styled.div`
  font-size: 40px;
  font-weight: 700;
  color: #fee715ff;
  margin-bottom: 40px;
`;

const DishesTypeContainer = styled.div`
  display: flex;
  flex-direction: column;
`;

const MealSection = styled.div`
  margin-bottom: 40px;
`;

const handleScrollTo = (name) => {
  scroller.scrollTo(name, {
    duration: 1500,
    delay: 100,
    offset: -50,
    smooth: true,
  });
};

const GoBackBtn = styled.span`
  color: #000;

  cursor: pointer;
`;

const CategoryItemWrapper = styled.div`
  margin: 0 10px;
  width: 70px;
  height: 120px;
  display: flex;
  flex-flow: column nowrap;
  justify-content: center;
  align-items: center;
  cursor: pointer;
`;

export const Menu= () => {
  const [t, i18n] = useTranslation("common");
  
  React.useEffect(() => {
    AOS.init();
  }, []);

  const getMealById = (id) => {
    let meal = meals.find((item) => item.id === id);
    return meal;
  };

  const [selectedMeals, setSelectedMeals] = React.useState([]);
  const addToCart = (id) => {
    let newMeal = getMealById(id);

    setSelectedMeals([...selectedMeals, newMeal]);
  };
  const removeFromCart = (id) => {
    setSelectedMeals(selectedMeals.filter((e) => e.id !== id));
  };
  return (
    <Layout>
      <Header>
        <Link
          style={{ textDecoration: "none", margin: "0" }}
          to="/wybierz-jezyk"
        >
          <GoBackBtn style={{ marginLeft: "20px" }}>
            {t("textInfo.goBack")}
          </GoBackBtn>
        </Link>
        <span
          style={{
            marginRight: "20px",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            cursor: "pointer",
          }}
          onClick={() => handleScrollTo("Wybrane")}
        >
          <CartIcon width="30px" height="30px">
            {" "}
          </CartIcon>
          ({selectedMeals.length})
        </span>
      </Header>
      <MealTypeContainer
        data-aos="fade-up"
        data-aos-duration="1000"
        data-aos-easing="ease-in-out"
        data-aos-mirror="true"
        data-aos-anchor-placement="top-center"
        data-aos-once="true"
      >
        <CategoriesContainer>
          <CategoryItemWrapper onClick={() => handleScrollTo("Przystawki")}>
            <StarterIcon width={"50px"} height={"50px"} />
            <a href="#">{t("categories.starters")}</a>
          </CategoryItemWrapper>
          <CategoryItemWrapper onClick={() => handleScrollTo("Zupy")}>
            <SoupIcon width={"50px"} height={"50px"} />
            <a href="#">{t("categories.soups")}</a>
          </CategoryItemWrapper>
          <CategoryItemWrapper onClick={() => handleScrollTo("DaniaMiesne")}>
            <MeatIcon width={"50px"} height={"50px"} />
            <a href="#">{t("categories.meatDishes")}</a>
          </CategoryItemWrapper>
          <CategoryItemWrapper onClick={() => handleScrollTo("Ryby")}>
            <FishIcon width={"50px"} height={"50px"} />
            <a href="#">{t("categories.fishes")}</a>
          </CategoryItemWrapper>
          <CategoryItemWrapper onClick={() => handleScrollTo("Napoje")}>
            <DrinkIcon width={"50px"} height={"50px"} />
            <a href="#">{t("categories.drinks")}</a>
          </CategoryItemWrapper>
        </CategoriesContainer>

        <MenuContentContainer>
          <MealSection>
            <Element name="Przystawki"></Element>
            <DishesTypeTitle>{t("categories.starters")}</DishesTypeTitle>
            <DishesTypeContainer>
              {meals
                .filter((x) => x.category === "starters")
                .map((item, index) => {
                  return (
                    <Card
                      key={index}
                      id={item.id}
                      removeFromCart={removeFromCart}
                      addToCart={addToCart}
                      title={t(item.title)}
                      description={t(item.description)}
                      price={t(item.price)}
                      onCart={item.onCart}
                    />
                  );
                })}
            </DishesTypeContainer>
          </MealSection>
          <MealSection>
            <Element name="Zupy"></Element>
            <DishesTypeTitle>{t("categories.soups")}</DishesTypeTitle>
            <DishesTypeContainer>
              {meals
                .filter((x) => x.category === "soups")
                .map((item, index) => {
                  return (
                    <Card
                      key={index}
                      id={item.id}
                      removeFromCart={removeFromCart}
                      addToCart={addToCart}
                      title={t(item.title)}
                      description={t(item.description)}
                      price={t(item.price)}
                      onCart={item.onCart}
                    />
                  );
                })}
            </DishesTypeContainer>
          </MealSection>
          <MealSection>
            <Element name="DaniaMiesne"></Element>
            <DishesTypeTitle>{t("categories.meatDishes")}</DishesTypeTitle>
            <DishesTypeContainer>
              {meals
                .filter((x) => x.category === "meatDishes")
                .map((item, index) => {
                  return (
                    <Card
                      key={index}
                      id={item.id}
                      removeFromCart={removeFromCart}
                      addToCart={addToCart}
                      title={t(item.title)}
                      description={t(item.description)}
                      price={t(item.price)}
                      onCart={item.onCart}
                    />
                  );
                })}
            </DishesTypeContainer>
          </MealSection>
          <MealSection>
            <Element name="Ryby"></Element>
            <DishesTypeTitle>{t("categories.fishes")}</DishesTypeTitle>
            <DishesTypeContainer>
              {meals
                .filter((x) => x.category === "fishes")
                .map((item, index) => {
                  return (
                    <Card
                      key={index}
                      id={item.id}
                      removeFromCart={removeFromCart}
                      addToCart={addToCart}
                      title={t(item.title)}
                      description={t(item.description)}
                      price={t(item.price)}
                      onCart={item.onCart}
                    />
                  );
                })}
            </DishesTypeContainer>
          </MealSection>
          <MealSection>
            <Element name="Napoje"></Element>
            <DishesTypeTitle>{t("categories.drinks")}</DishesTypeTitle>
            <DishesTypeContainer>
              {meals
                .filter((x) => x.category === "drinks")
                .map((item, index) => {
                  return (
                    <Card
                      key={index}
                      id={item.id}
                      removeFromCart={removeFromCart}
                      addToCart={addToCart}
                      title={t(item.title)}
                      description={t(item.description)}
                      price={t(item.price)}
                      onCart={item.onCart}
                    />
                  );
                })}
            </DishesTypeContainer>
          </MealSection>
          {selectedMeals.length > 0 && (
            <MealSection>
              <Element name="Wybrane"></Element>
              <DishesTypeTitle>{t("categories.selected")}</DishesTypeTitle>
              <DishesTypeContainer>
                {selectedMeals.map((item, index) => (
                  <Card
                    key={index}
                    id={item.id}
                    title={t(item.title)}
                    description={t(item.description)}
                    price={t(item.price)}
                    removeFromCart={removeFromCart}
                    onCart
                  />
                ))}
              </DishesTypeContainer>
            </MealSection>
          )}
        </MenuContentContainer>
      </MealTypeContainer>
      <ScrollToTopIcon
        onClick={scroll.scrollToTop}
        data-aos="fade-up"
        data-aos-offset="200"
        data-aos-delay="50"
        data-aos-duration="1000"
        data-aos-easing="ease-in-out"
        data-aos-mirror="true"
        data-aos-once="false"
        data-aos-anchor-placement="top-center"
      >
        <img src={up} width="100%" style={{ opacity: "0.5" }} />{" "}
      </ScrollToTopIcon>
    </Layout>
  );
};
